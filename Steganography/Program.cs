﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Steganography
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Random rnd = new Random();

            //Bitmap myim = new Bitmap(@"C:\Users\Admin\YandexDisk\Сети\Steganography\Stoun_b.bmp");

            //LSBMethod mask = new LSBMethod(myim);

            //string input = new string('x',1000);
            ////for (int i=0;i<1000000;i++)
            ////{
            ////    input += (byte)rnd.Next(0, 255);
            ////}

            //mask.Encrypt(input);

            //string output = mask.Decrypt() as string;

            //bool isOk = (input == output) && input.Length == output.Length;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainWindow());
        }
    }
}
