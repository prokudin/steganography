﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Steganography
{
    class BitOperation
    {
        // перевод из байта в биты
        public static BitArray ByteToBit(byte src)
        {
            BitArray bitArray = new BitArray(8);
            
            for (int i = 0; i < 8; i++)
            {
                bitArray[i] = (src >> i & 1) == 1 ;
            }
            return bitArray;
        }

        // перевод из битов в байт
        public static byte BitsToByte(ref BitArray bits)
        {
            byte num = 0;

            for (int i = 0,size = bits.Count; i < size; i++)
                if (bits[i])
                    num += (byte)Math.Pow(2, i);

            return num;
        }


        // перевод из битов в байт
        public static byte BitsToByte(ref BitArray bits, int from, int to)
        {
            if (to > bits.Length-1 || from > to || from < 0)
                throw new Exception("Illegal Argument");

            byte num = 0;            

            for (int i = from, pow = 0; i <= to; i++, pow++)
                if (bits[i])
                    num += (byte)Math.Pow(2, pow);

            return num;
        }
    }
}
