﻿namespace Steganography
{
    partial class MainWindow
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            this.originalPicture = new System.Windows.Forms.PictureBox();
            this.modifiedPicture = new System.Windows.Forms.PictureBox();
            this.deltaPicture = new System.Windows.Forms.PictureBox();
            this.openOriginalPicture = new System.Windows.Forms.Button();
            this.saveOriginalPicture = new System.Windows.Forms.Button();
            this.text = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.saveModifieldPicture = new System.Windows.Forms.Button();
            this.openModifieldPicture = new System.Windows.Forms.Button();
            this.saveDeltaPicture = new System.Windows.Forms.Button();
            this.encryptMethods = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.encryptOriginalImage = new System.Windows.Forms.Button();
            this.decryptModifiedImage = new System.Windows.Forms.Button();
            this.findDeltaImage = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.attackMethods = new System.Windows.Forms.ComboBox();
            this.attack = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.copyOriginalToModified = new System.Windows.Forms.Button();
            this.copyModifiedToOriginal = new System.Windows.Forms.Button();
            this.analysisMethod = new System.Windows.Forms.ComboBox();
            this.analyse = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.analysisMessage = new System.Windows.Forms.TextBox();
            this.encryptImage = new System.Windows.Forms.Button();
            this.decryptImage = new System.Windows.Forms.Button();
            this.clearEncrypting = new System.Windows.Forms.Button();
            this.superAttack = new System.Windows.Forms.Button();
            this.analisysDinamic = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.originalPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.modifiedPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deltaPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.analisysDinamic)).BeginInit();
            this.SuspendLayout();
            // 
            // originalPicture
            // 
            this.originalPicture.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.originalPicture.Location = new System.Drawing.Point(33, 21);
            this.originalPicture.Name = "originalPicture";
            this.originalPicture.Size = new System.Drawing.Size(307, 337);
            this.originalPicture.TabIndex = 0;
            this.originalPicture.TabStop = false;
            // 
            // modifiedPicture
            // 
            this.modifiedPicture.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.modifiedPicture.Location = new System.Drawing.Point(382, 21);
            this.modifiedPicture.Name = "modifiedPicture";
            this.modifiedPicture.Size = new System.Drawing.Size(307, 337);
            this.modifiedPicture.TabIndex = 1;
            this.modifiedPicture.TabStop = false;
            // 
            // deltaPicture
            // 
            this.deltaPicture.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.deltaPicture.Location = new System.Drawing.Point(735, 21);
            this.deltaPicture.Name = "deltaPicture";
            this.deltaPicture.Size = new System.Drawing.Size(307, 337);
            this.deltaPicture.TabIndex = 2;
            this.deltaPicture.TabStop = false;
            // 
            // openOriginalPicture
            // 
            this.openOriginalPicture.Location = new System.Drawing.Point(33, 364);
            this.openOriginalPicture.Name = "openOriginalPicture";
            this.openOriginalPicture.Size = new System.Drawing.Size(75, 23);
            this.openOriginalPicture.TabIndex = 3;
            this.openOriginalPicture.Text = "Открыть";
            this.openOriginalPicture.UseVisualStyleBackColor = true;
            this.openOriginalPicture.Click += new System.EventHandler(this.openOriginalPicture_Click);
            // 
            // saveOriginalPicture
            // 
            this.saveOriginalPicture.Location = new System.Drawing.Point(265, 364);
            this.saveOriginalPicture.Name = "saveOriginalPicture";
            this.saveOriginalPicture.Size = new System.Drawing.Size(75, 23);
            this.saveOriginalPicture.TabIndex = 4;
            this.saveOriginalPicture.Text = "Сохранить";
            this.saveOriginalPicture.UseVisualStyleBackColor = true;
            this.saveOriginalPicture.Click += new System.EventHandler(this.saveOriginalPicture_Click);
            // 
            // text
            // 
            this.text.Location = new System.Drawing.Point(33, 526);
            this.text.MaxLength = 2147483647;
            this.text.Multiline = true;
            this.text.Name = "text";
            this.text.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.text.Size = new System.Drawing.Size(700, 91);
            this.text.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(110, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(147, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Изображение до шифровки";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(453, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(165, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Изображение после шифровки";
            // 
            // saveModifieldPicture
            // 
            this.saveModifieldPicture.Location = new System.Drawing.Point(614, 364);
            this.saveModifieldPicture.Name = "saveModifieldPicture";
            this.saveModifieldPicture.Size = new System.Drawing.Size(75, 23);
            this.saveModifieldPicture.TabIndex = 10;
            this.saveModifieldPicture.Text = "Сохранить";
            this.saveModifieldPicture.UseVisualStyleBackColor = true;
            this.saveModifieldPicture.Click += new System.EventHandler(this.saveModifieldPicture_Click);
            // 
            // openModifieldPicture
            // 
            this.openModifieldPicture.Location = new System.Drawing.Point(382, 364);
            this.openModifieldPicture.Name = "openModifieldPicture";
            this.openModifieldPicture.Size = new System.Drawing.Size(75, 23);
            this.openModifieldPicture.TabIndex = 9;
            this.openModifieldPicture.Text = "Открыть";
            this.openModifieldPicture.UseVisualStyleBackColor = true;
            this.openModifieldPicture.Click += new System.EventHandler(this.openModifiedPicture_Click);
            // 
            // saveDeltaPicture
            // 
            this.saveDeltaPicture.Location = new System.Drawing.Point(912, 364);
            this.saveDeltaPicture.Name = "saveDeltaPicture";
            this.saveDeltaPicture.Size = new System.Drawing.Size(75, 23);
            this.saveDeltaPicture.TabIndex = 12;
            this.saveDeltaPicture.Text = "Сохранить";
            this.saveDeltaPicture.UseVisualStyleBackColor = true;
            this.saveDeltaPicture.Click += new System.EventHandler(this.saveDeltaPicture_Click);
            // 
            // encryptMethods
            // 
            this.encryptMethods.FormattingEnabled = true;
            this.encryptMethods.Location = new System.Drawing.Point(153, 430);
            this.encryptMethods.Name = "encryptMethods";
            this.encryptMethods.Size = new System.Drawing.Size(121, 21);
            this.encryptMethods.TabIndex = 13;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(41, 430);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Метод шифрования";
            // 
            // encryptOriginalImage
            // 
            this.encryptOriginalImage.Location = new System.Drawing.Point(128, 364);
            this.encryptOriginalImage.Name = "encryptOriginalImage";
            this.encryptOriginalImage.Size = new System.Drawing.Size(116, 23);
            this.encryptOriginalImage.TabIndex = 15;
            this.encryptOriginalImage.Text = "Зашифровать текст";
            this.encryptOriginalImage.UseVisualStyleBackColor = true;
            this.encryptOriginalImage.Click += new System.EventHandler(this.encryptOriginalImage_Click);
            // 
            // decryptModifiedImage
            // 
            this.decryptModifiedImage.Location = new System.Drawing.Point(470, 364);
            this.decryptModifiedImage.Name = "decryptModifiedImage";
            this.decryptModifiedImage.Size = new System.Drawing.Size(132, 23);
            this.decryptModifiedImage.TabIndex = 16;
            this.decryptModifiedImage.Text = "Расшифровать текст";
            this.decryptModifiedImage.UseVisualStyleBackColor = true;
            this.decryptModifiedImage.Click += new System.EventHandler(this.decryptModifiedImage_Click);
            // 
            // findDeltaImage
            // 
            this.findDeltaImage.Location = new System.Drawing.Point(760, 364);
            this.findDeltaImage.Name = "findDeltaImage";
            this.findDeltaImage.Size = new System.Drawing.Size(121, 23);
            this.findDeltaImage.TabIndex = 17;
            this.findDeltaImage.Text = "Рассчитать разницу";
            this.findDeltaImage.UseVisualStyleBackColor = true;
            this.findDeltaImage.Click += new System.EventHandler(this.findDeltaImage_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(41, 509);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Сообщение";
            // 
            // attackMethods
            // 
            this.attackMethods.FormattingEnabled = true;
            this.attackMethods.Location = new System.Drawing.Point(463, 456);
            this.attackMethods.Name = "attackMethods";
            this.attackMethods.Size = new System.Drawing.Size(133, 21);
            this.attackMethods.TabIndex = 19;
            // 
            // attack
            // 
            this.attack.Location = new System.Drawing.Point(605, 456);
            this.attack.Name = "attack";
            this.attack.Size = new System.Drawing.Size(75, 23);
            this.attack.TabIndex = 20;
            this.attack.Text = "Атаковать";
            this.attack.UseVisualStyleBackColor = true;
            this.attack.Click += new System.EventHandler(this.attack_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(386, 459);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 13);
            this.label6.TabIndex = 21;
            this.label6.Text = "Метод атаки";
            // 
            // copyOriginalToModified
            // 
            this.copyOriginalToModified.Location = new System.Drawing.Point(347, 110);
            this.copyOriginalToModified.Name = "copyOriginalToModified";
            this.copyOriginalToModified.Size = new System.Drawing.Size(29, 23);
            this.copyOriginalToModified.TabIndex = 22;
            this.copyOriginalToModified.Text = "=>";
            this.copyOriginalToModified.UseVisualStyleBackColor = true;
            this.copyOriginalToModified.Click += new System.EventHandler(this.copyOriginalToModified_Click);
            // 
            // copyModifiedToOriginal
            // 
            this.copyModifiedToOriginal.Location = new System.Drawing.Point(347, 220);
            this.copyModifiedToOriginal.Name = "copyModifiedToOriginal";
            this.copyModifiedToOriginal.Size = new System.Drawing.Size(29, 23);
            this.copyModifiedToOriginal.TabIndex = 23;
            this.copyModifiedToOriginal.Text = "<=";
            this.copyModifiedToOriginal.UseVisualStyleBackColor = true;
            this.copyModifiedToOriginal.Click += new System.EventHandler(this.copyModifiedToOriginal_Click);
            // 
            // analysisMethod
            // 
            this.analysisMethod.FormattingEnabled = true;
            this.analysisMethod.Location = new System.Drawing.Point(760, 431);
            this.analysisMethod.Name = "analysisMethod";
            this.analysisMethod.Size = new System.Drawing.Size(121, 21);
            this.analysisMethod.TabIndex = 25;
            // 
            // analyse
            // 
            this.analyse.Location = new System.Drawing.Point(912, 429);
            this.analyse.Name = "analyse";
            this.analyse.Size = new System.Drawing.Size(103, 23);
            this.analyse.TabIndex = 26;
            this.analyse.Text = "Анализировать";
            this.analyse.UseVisualStyleBackColor = true;
            this.analyse.Click += new System.EventHandler(this.analyse_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(751, 409);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 13);
            this.label7.TabIndex = 27;
            this.label7.Text = "Метод анализа";
            // 
            // analysisMessage
            // 
            this.analysisMessage.Location = new System.Drawing.Point(760, 459);
            this.analysisMessage.Name = "analysisMessage";
            this.analysisMessage.ReadOnly = true;
            this.analysisMessage.Size = new System.Drawing.Size(255, 20);
            this.analysisMessage.TabIndex = 28;
            // 
            // encryptImage
            // 
            this.encryptImage.Location = new System.Drawing.Point(113, 393);
            this.encryptImage.Name = "encryptImage";
            this.encryptImage.Size = new System.Drawing.Size(160, 23);
            this.encryptImage.TabIndex = 29;
            this.encryptImage.Text = "Зашифровать изображение";
            this.encryptImage.UseVisualStyleBackColor = true;
            this.encryptImage.Click += new System.EventHandler(this.encryptImage_Click);
            // 
            // decryptImage
            // 
            this.decryptImage.Location = new System.Drawing.Point(456, 393);
            this.decryptImage.Name = "decryptImage";
            this.decryptImage.Size = new System.Drawing.Size(162, 23);
            this.decryptImage.TabIndex = 30;
            this.decryptImage.Text = "Расшифровать изображение";
            this.decryptImage.UseVisualStyleBackColor = true;
            this.decryptImage.Click += new System.EventHandler(this.decryptImage_Click);
            // 
            // clearEncrypting
            // 
            this.clearEncrypting.Location = new System.Drawing.Point(456, 422);
            this.clearEncrypting.Name = "clearEncrypting";
            this.clearEncrypting.Size = new System.Drawing.Size(173, 23);
            this.clearEncrypting.TabIndex = 31;
            this.clearEncrypting.Text = "Очистить следы шифрования";
            this.clearEncrypting.UseVisualStyleBackColor = true;
            this.clearEncrypting.Click += new System.EventHandler(this.clearEncrypting_Click);
            // 
            // superAttack
            // 
            this.superAttack.Location = new System.Drawing.Point(463, 483);
            this.superAttack.Name = "superAttack";
            this.superAttack.Size = new System.Drawing.Size(139, 23);
            this.superAttack.TabIndex = 33;
            this.superAttack.Text = "Множественная атака";
            this.superAttack.UseVisualStyleBackColor = true;
            this.superAttack.Click += new System.EventHandler(this.superAttack_Click);
            // 
            // analisysDinamic
            // 
            chartArea1.Name = "ChartArea1";
            this.analisysDinamic.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.analisysDinamic.Legends.Add(legend1);
            this.analisysDinamic.Location = new System.Drawing.Point(760, 526);
            this.analisysDinamic.Name = "analisysDinamic";
            this.analisysDinamic.Size = new System.Drawing.Size(255, 91);
            this.analisysDinamic.TabIndex = 34;
            this.analisysDinamic.Text = "chart1";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1077, 709);
            this.Controls.Add(this.analisysDinamic);
            this.Controls.Add(this.superAttack);
            this.Controls.Add(this.clearEncrypting);
            this.Controls.Add(this.decryptImage);
            this.Controls.Add(this.encryptImage);
            this.Controls.Add(this.analysisMessage);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.analyse);
            this.Controls.Add(this.analysisMethod);
            this.Controls.Add(this.copyModifiedToOriginal);
            this.Controls.Add(this.copyOriginalToModified);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.attack);
            this.Controls.Add(this.attackMethods);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.findDeltaImage);
            this.Controls.Add(this.decryptModifiedImage);
            this.Controls.Add(this.encryptOriginalImage);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.encryptMethods);
            this.Controls.Add(this.saveDeltaPicture);
            this.Controls.Add(this.saveModifieldPicture);
            this.Controls.Add(this.openModifieldPicture);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.text);
            this.Controls.Add(this.saveOriginalPicture);
            this.Controls.Add(this.openOriginalPicture);
            this.Controls.Add(this.deltaPicture);
            this.Controls.Add(this.modifiedPicture);
            this.Controls.Add(this.originalPicture);
            this.Name = "MainWindow";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.originalPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.modifiedPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deltaPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.analisysDinamic)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox originalPicture;
        private System.Windows.Forms.PictureBox modifiedPicture;
        private System.Windows.Forms.PictureBox deltaPicture;
        private System.Windows.Forms.Button openOriginalPicture;
        private System.Windows.Forms.Button saveOriginalPicture;
        private System.Windows.Forms.TextBox text;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button saveModifieldPicture;
        private System.Windows.Forms.Button openModifieldPicture;
        private System.Windows.Forms.Button saveDeltaPicture;
        private System.Windows.Forms.ComboBox encryptMethods;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button encryptOriginalImage;
        private System.Windows.Forms.Button decryptModifiedImage;
        private System.Windows.Forms.Button findDeltaImage;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox attackMethods;
        private System.Windows.Forms.Button attack;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button copyOriginalToModified;
        private System.Windows.Forms.Button copyModifiedToOriginal;
        private System.Windows.Forms.ComboBox analysisMethod;
        private System.Windows.Forms.Button analyse;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox analysisMessage;
        private System.Windows.Forms.Button encryptImage;
        private System.Windows.Forms.Button decryptImage;
        private System.Windows.Forms.Button clearEncrypting;
        private System.Windows.Forms.Button superAttack;
        private System.Windows.Forms.DataVisualization.Charting.Chart analisysDinamic;
    }
}

