﻿using Steganography.Attack;
using Steganography.Analysis;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Steganography
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();

            /// добавить методы шифрования в комбо-боксы
            encryptMethods.Items.AddRange(stenographyMethods);
            encryptMethods.SelectedIndex = 6;
            encryptMethods.DropDownStyle = ComboBoxStyle.DropDownList;

            // добавить методы атаки
            attackMethods.Items.AddRange(new object[] 
            { new BrightnessAttack(),
               new BlurAttack(),
               new KontrAttack(),
               new MashtabAttack(),
               new NotBriAttack(),
               new ShumAttack(),
               new CSVBlur(),
               new CSVRotate()
            });
            attackMethods.SelectedIndex = 0;
            attackMethods.DropDownStyle = ComboBoxStyle.DropDownList;

            // добавить методы анализа
            analysisMethod.Items.AddRange(analysisMethods);
            analysisMethod.SelectedIndex = 0;
            analysisMethod.DropDownStyle = ComboBoxStyle.DropDownList;


            // добавить методы ошибок в диаграмму
            //for (int i=0;i< analysisMethods.Length; i++)
            //{
            //    analisysDinamic.Series.Add(analysisMethods[i].ToString());
            //}

            setEnabled();
        }


        private IAnalysisMethod[] analysisMethods = 
        {    new AveragePixelError(),
             new BitErrorRate(),
             new MeanSquareError(),
             new PeakSignalToNoiseRatio(),
             new RootMeanSquare(),
             new NormalizedCrossCorrelation(),
             new SSIM()
        };

        private IStenographyMethod[] stenographyMethods =
        {    new LSBMethod(),
             new LSBMethod2(),
             new KutterJordanBossen(),
             new PseudoRandomPermutation(),
             new PseudoRandomInterval(),
             new BlockHidingMethod(),
             new AbstractDigitalWatermarkingDWT()
        };

        private Bitmap originalImage = null; 
        private Bitmap modifiedImage = null;
        private Bitmap infoImage = null;

        private Bitmap beforeEncrypt = null;
        private Bitmap arterDecrypt = null;


        /// <summary>
        /// Открывает изображение и сохраняет его в picture
        /// </summary>
        /// <param name="picture">Картинка</param>
        /// <exception cref="Exception"></exception>
        private void OpenPicture(ref Bitmap picture) 
        {    
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = "c:\\Avatars";
            openFileDialog1.Filter = "jpeg files (*.jpg)|*.jpg|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    Stream str = openFileDialog1.OpenFile();
                    picture = new Bitmap(str);
                    str.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Ошибка : невозможно считать файл с диска." + ex.Message);
                }
            }
        }

        /// <summary>
        /// Сохраняет картинку
        /// </summary>
        /// <param name="picture">Картинка</param>        /// 
        /// <exception cref="Exception"></exception>
        private void SavePicture(Bitmap picture)
        {
            if (picture == null)
            {
                throw new Exception("Пустое изображение. Пожалуйста, откройте изображение перед сохранением");
            }

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "jpeg files (*.jpg)|*.jpg|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    picture.Save(saveFileDialog1.FileName);
                }
                catch(Exception ex)
                {
                    throw new Exception("Ошибка : невозможо сохранить файл на диск. " + ex.Message);
                }
            }
        }

        /// <summary>
        /// Шифрует оригинальную картинку
        /// </summary>
        /// <param name="method">Метод шифрования</param>
        /// <param name="message">Сообщение</param>
        /// <exception cref="Exception"></exception>
        private Bitmap Encrypt(IStenographyMethod method,object message,DataType type,ref Bitmap image)
        {
			if (image == null)
            {
                throw new Exception("Невозможно зашифровать пустую картинку.");
            }			
			
            Bitmap result = new Bitmap(image);            

            try
            {                 
                method.Image = result;

                switch (type)
                {
                    case DataType.TEXT:
                        method.Encrypt<string>(message as string);
                        break;
                    case DataType.IMAGE:
                        method.Encrypt<Bitmap>(message as Bitmap);
                        break;
                }

                result = method.Image;
            }
            catch (Exception ex)
            {
                throw new Exception("Невозможно зашифровать картинку. " + ex.Message);
            }

            return result;
        }
        
        /// <summary>
        /// Дешифрует зашифрованную картинку
        /// </summary>
        /// <param name="method">Метод шафрования</param>
        /// <returns>дешифрованная строка</returns>        
        /// <exception cref="Exception"></exception>
        private object Decrypt(IStenographyMethod method, DataType type,ref Bitmap image)
        {
            method.Image = image ?? throw new Exception("Невозможно расшифровать пустую картинку");

            
            object result= null;

            try
            {
                switch (type)
                {
                    case DataType.TEXT:
                        result = method.Decrypt<string>();
                        break;
                    case DataType.IMAGE:
                        result = method.Decrypt<Bitmap>();
                        break;
                }
                

                if (result == null)
                    throw new Exception("Исходное изображение не зашифровано");
            }
            catch (Exception ex)
            {
                throw new Exception("Ошибка: навозможно расшифровать изображение. " + ex.Message);//MessageBox.Show("Error: Could not encrypt image. Original error: " + ex.Message);
            }

            return result;
        }

        /// <summary>
        /// Находит разницу между изображениями
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        private Bitmap FindDelta(ref Bitmap first, ref Bitmap second)
        {
            if (first == null || second == null)
            {
                throw new Exception("Error: Could not find delta. You must load 2 images: ");            
            }

            
            Bitmap result = new Bitmap(first.Width,first.Height);

            //byte deltaR, deltaG, deltaB;
            Color color1, color2/*, nColor*/;
            for (int i=0, height = first.Height, width = first.Width; i< width; i++)
            {
                for (int j=0;j< height; j++)
                {
                    color1 = first.GetPixel(i, j);
                    color2 = second.GetPixel(i, j);

                    //deltaR = (byte)(Math.Abs(color1.R - color2.R) * 50);
                    //deltaG = (byte)(Math.Abs(color1.G - color2.G) * 50);
                    //deltaB = (byte)(Math.Abs(color1.B - color2.B) * 50);

                   
                    //nColor = Color.FromArgb((byte)(Math.Abs(color1.R - color2.R) * 50), (byte)(Math.Abs(color1.G - color2.G) * 50), (byte)(Math.Abs(color1.B - color2.B) * 50));

                    result.SetPixel(i, j, Color.FromArgb((byte)(Math.Abs(color1.R - color2.R) * 36), (byte)(Math.Abs(color1.G - color2.G) * 36), (byte)(Math.Abs(color1.B - color2.B) * 36)));
                }
            }

            return result;
        }

        /// <summary>
        /// Видимость виджетов
        /// </summary>
        private void setEnabled()
        {
            encryptOriginalImage.Enabled = originalImage != null;
            saveOriginalPicture.Enabled = originalImage != null;
            copyOriginalToModified.Enabled = originalImage != null;

            decryptModifiedImage.Enabled = modifiedImage != null;
            saveModifieldPicture.Enabled = modifiedImage != null;
            attack.Enabled = modifiedImage != null;
            copyModifiedToOriginal.Enabled = modifiedImage != null;
            clearEncrypting.Enabled = modifiedImage != null;

            saveDeltaPicture.Enabled = infoImage != null;

            findDeltaImage.Enabled = originalImage != null && modifiedImage != null;
            
            analysisMessage.Enabled = originalImage != null && modifiedImage != null;
            analyse.Enabled = originalImage != null && modifiedImage != null;

            encryptImage.Enabled = originalImage != null;
            decryptImage.Enabled = modifiedImage != null;

            superAttack.Enabled = modifiedImage != null;
        }

        #region Слоты для кнопок
        
        private void openOriginalPicture_Click(object sender, EventArgs e)
        {
            try
            {
                OpenPicture(ref originalImage);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            originalPicture.Image = originalImage;
            setEnabled();
        }

        private void saveOriginalPicture_Click(object sender, EventArgs e)
        {
            try
            {
                SavePicture(originalImage);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void openModifiedPicture_Click(object sender, EventArgs e)
        {
            try
            { 
                OpenPicture(ref modifiedImage);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            modifiedPicture.Image = modifiedImage;

            setEnabled();
        }

        private void saveModifieldPicture_Click(object sender, EventArgs e)
        {
            try
            {
                SavePicture(modifiedImage);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void encryptOriginalImage_Click(object sender, EventArgs e)
        {
            string txt = text.Text;

            if (txt.Length == 0)
            {
                MessageBox.Show("String is empty");
                return;
            }

            try
            {
                modifiedImage=Encrypt(encryptMethods.SelectedItem as IStenographyMethod, text.Text,DataType.TEXT,ref originalImage);
                modifiedPicture.Image = modifiedImage;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            setEnabled();
        }

        private void decryptModifiedImage_Click(object sender, EventArgs e)
        {
            text.Text = "";

            try
            {
                text.Text = Decrypt(encryptMethods.SelectedItem as IStenographyMethod,DataType.TEXT, ref modifiedImage) as string;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            setEnabled();
        }

        private void findDeltaImage_Click(object sender, EventArgs e)
        {
            try
            {
                infoImage = FindDelta(ref originalImage, ref modifiedImage);
                deltaPicture.Image = infoImage;
            }
            catch( Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            setEnabled();
        }

        private void saveDeltaPicture_Click(object sender, EventArgs e)
        {
            try
            {
                SavePicture(infoImage);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void attack_Click(object sender, EventArgs e)
        {
            Bitmap map = (attackMethods.SelectedItem as IAttackMethod).Attack(modifiedImage);

            if (map!=null)
            {
                modifiedImage = map;
                modifiedPicture.Image = map;
            }
        }

        private void copyOriginalToModified_Click(object sender, EventArgs e)
        {
            if (originalImage == null)
                throw new Exception("Original image is empty");

            modifiedImage = new Bitmap(originalImage);
            modifiedPicture.Image = modifiedImage;

            setEnabled();
        }

        private void copyModifiedToOriginal_Click(object sender, EventArgs e)
        {
            if (modifiedImage == null)
                throw new Exception("Modified image is empty");

            originalImage = new Bitmap(modifiedImage);
            originalPicture.Image = originalImage;

            setEnabled();
        }

        private void analyse_Click(object sender, EventArgs e)
        {            
            string resultStr = "";

            try
            {
                if ((analysisMethod.SelectedItem is SSIM) || (analysisMethod.SelectedItem is NormalizedCrossCorrelation))
                {
                    if (beforeEncrypt == null)
                        beforeEncrypt = originalImage;

                    if (arterDecrypt == null)
                        decryptImage_Click(null, null);

                    resultStr = (analysisMethod.SelectedItem as IAnalysisMethod).Analyse( beforeEncrypt,  arterDecrypt).ToString();
                }
                else
                {
                    resultStr = (analysisMethod.SelectedItem as IAnalysisMethod).Analyse( originalImage,  modifiedImage).ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Невозможно найти метрики. Оригинальная ошибка" + ex.Message);
            }

            analysisMessage.Text = resultStr;
        }

		private void encryptImage_Click(object sender, EventArgs e)
        {
            try
            {
                OpenPicture(ref beforeEncrypt);

                if (beforeEncrypt == null)
                    return;

                modifiedImage = Encrypt(encryptMethods.SelectedItem as IStenographyMethod, beforeEncrypt, DataType.IMAGE, ref originalImage);
                modifiedPicture.Image = modifiedImage;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            setEnabled();
        }

        private void decryptImage_Click(object sender, EventArgs e)
        {
            try
            {
                arterDecrypt = Decrypt(encryptMethods.SelectedItem as IStenographyMethod, DataType.IMAGE, ref modifiedImage) as Bitmap;

                infoImage = arterDecrypt;
                deltaPicture.Image = arterDecrypt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            setEnabled();
        }

        private void clearEncrypting_Click(object sender, EventArgs e)
        {
            IStenographyMethod method = encryptMethods.SelectedItem as IStenographyMethod;

            method.Clear();
            modifiedImage = method.Image;
            modifiedPicture.Image = modifiedImage;
        }


        #endregion

        private void superAttack_Click(object sender, EventArgs e)
        {
            analisysDinamic.Series.Clear();

            analisysDinamic.Invalidate();

            var series = new Series() {
                ChartArea = "ChartArea1",
                Name = (analysisMethod.SelectedItem as IAnalysisMethod).ToString(),
                Color = System.Drawing.Color.Green,
                IsVisibleInLegend = false,
                IsXValueIndexed = true,
                ChartType = SeriesChartType.Line
            };
            

            analisysDinamic.Series.Add(series);

            double y;

            Bitmap image = new Bitmap(modifiedImage);
            double minY = 100000000, maxY = -10000000;

            for (int i=0;i<10;i++)
            {
                image = (attackMethods.SelectedItem as IAttackMethod).Attack(image);

                modifiedPicture.Image = image;

                y = (analysisMethod.SelectedItem as IAnalysisMethod).Analyse(modifiedImage,image);

                if (y < minY)
                    minY = y;

                if (y> maxY)
                    maxY = y;

                series.Points.AddXY(i, y);
            }

            analisysDinamic.ChartAreas[0].AxisY.Maximum = maxY!=0 ? maxY * 1.2:1;
            analisysDinamic.ChartAreas[0].AxisY.Minimum = minY!=0 ? minY * 0.8 : 0;

            analisysDinamic.Invalidate();
        }

        
    }
}
