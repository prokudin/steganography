﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;




namespace Steganography.Attack
{
    class MashtabAttack : IAttackMethod
    {
        public Bitmap Attack(Bitmap image)
        {
            int width = (int)(image.Width * 1.05);
            int height = (int)(image.Height * 1.05);

            Size nSize = new Size(width, height);
            Bitmap gdi = new Bitmap(nSize.Width, nSize.Height);
            Graphics ZoomInGraphics = Graphics.FromImage(gdi);
            ZoomInGraphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
            ZoomInGraphics.DrawImage(image, new Rectangle(new Point(0, 0), nSize), new Rectangle(new Point(0, 0), image.Size), GraphicsUnit.Pixel);
            ZoomInGraphics.Dispose();
            return gdi;
        }

        public override string ToString()
        {
            return "Атака масштабированием";
        }
    }
}
