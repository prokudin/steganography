﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Steganography.Attack
{
    class NotBriAttack : IAttackMethod
    {

        public Bitmap Attack( Bitmap image)
        {
            Bitmap result = new Bitmap(image);

            float factor = 0.95f;
            int width = image.Width;
            int height = image.Height;
            float R, G, B;
            Color current;
            for (int i = 0, j = 0; i < width; i++)
            {
                for (j = 0; j < height; j++)
                {
                    current = image.GetPixel(i, j);

                    R = (factor * current.R);
                    G = (factor * current.G);
                    B = (factor * current.B);

                    result.SetPixel(i, j, Color.FromArgb(R < 0 ? 0 : (byte)R, G < 0 ? 0 : (byte)G, B < 0 ? 0 : (byte)B));
                }
            }

            return result;
        }


        public override string ToString()
        {
            return "Атака затемнением";
        }
    }
}
