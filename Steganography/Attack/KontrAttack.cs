﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Steganography.Attack
{
    class KontrAttack : IAttackMethod
    {

        public Bitmap Attack(Bitmap image)
        {
            Bitmap result = new Bitmap(image);
           
            int width = image.Width;
            int height = image.Height;
            int R, G, B;
            Color current;
            for(int i=0,j=0;i< width;i++)
            {
                for (j=0;j<height;j++)
                {
                    current = image.GetPixel(i, j);
                    if (current.R > 125 && current.R < 250)
                        R = current.R + 5;
                    else if (current.R < 126 && current.R >= 5)
                        R = current.R - 5;
                    else
                        R = current.R;

                    if (current.G > 125 && current.G < 250)
                        G = current.G + 5;
                    else if (current.G < 126 && current.G >= 5)
                       G = current.G - 5;
                    else
                        G = current.G;

                    if (current.B > 125 && current.B < 250)
                        B = current.B + 5;
                    else if (current.B < 126 && current.B >= 5)
                        B = current.B - 5;
                    else
                        B = current.B;


                    result.SetPixel(i, j, Color.FromArgb(R, G, B));
                }
            }

            return result;
        }


        public override string ToString()
        {
            return "Атака контрастом";
        }
    }
}
