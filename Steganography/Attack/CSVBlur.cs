﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Emgu.CV;
using Emgu.CV.Stitching;
using Emgu.CV.Structure;
using System.Drawing;

namespace Steganography.Attack
{
    class CSVBlur : IAttackMethod
    {      
        public Bitmap Attack( Bitmap image)
        {
            Image<Bgr, byte> newimage = new Image<Bgr, byte>(image);

            newimage = newimage.SmoothGaussian(3, 3, 34.3, 45.3);

            return newimage.ToBitmap();
        }

        public override string ToString()
        {
            return "CSVBlur";
        }
    }
}