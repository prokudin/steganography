﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;




namespace Steganography.Attack
{
    class ShumAttack : IAttackMethod
    {
        static int seed = 1000;

        public Bitmap Attack(Bitmap image)
        {
            seed++;

            Bitmap result = new Bitmap(image.Width, image.Height);
            result = image;
            Random r = new Random(seed);

            for (int i = 0; i < image.Width; i += r.Next(2, 8))
            {
                for (int j = 0; j < image.Height; j += r.Next(1, 9))
                {
                    int num = r.Next(0, 255);
                    result.SetPixel(i, j, Color.FromArgb(255, num, num, num));
                }
            }
            return result;
        }

        public override string ToString()
        {
            return "Атака шумом";
        }
    }
}
