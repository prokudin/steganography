﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.Stitching;
using Emgu.CV.Structure;

namespace Steganography.Attack
{
    class CSVRotate : IAttackMethod
    {
        public Bitmap Attack(Bitmap image)
        {
            Image<Bgr, byte> newimage = new Image<Bgr, byte>(image);

            newimage = newimage.Rotate(0.1,new Bgr(image.GetPixel(0,0)));

            return newimage.ToBitmap();
        }


        public override string ToString()
        {
            return "CSVRotate";
        }
    }
}