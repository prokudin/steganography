﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;





namespace Steganography.Attack
{
    class BlurAttack : IAttackMethod
    {

        public Bitmap Attack(Bitmap image)
        {
            Bitmap result = new Bitmap(image.Width, image.Height);
            result = image;

            // horizontal blur
            for (int i = 1; i < image.Width - 1; i++)
                for (int j = 0; j < image.Height; j++)
                {
                    Color c1 = image.GetPixel(i - 1, j);
                    Color c2 = image.GetPixel(i, j);
                    Color c3 = image.GetPixel(i + 1, j);


                    byte bR = (byte)((c1.R + c2.R + c3.R) / 3);
                    byte bG = (byte)((c1.G + c2.G + c3.G) / 3);
                    byte bB = (byte)((c1.B + c2.B + c3.B) / 3);


                    Color cBlured = Color.FromArgb(bR, bG, bB);

                    result.SetPixel(i, j, cBlured);

                }

            //vertical blur
            for (int i = 0; i < image.Width; i++)
                for (int j = 1; j < image.Height - 1; j++)
                {
                    Color c1 = image.GetPixel(i, j - 1);
                    Color c2 = image.GetPixel(i, j);
                    Color c3 = image.GetPixel(i, j + 1);


                    byte bR = (byte)((c1.R + c2.R + c3.R) / 3);
                    byte bG = (byte)((c1.G + c2.G + c3.G) / 3);
                    byte bB = (byte)((c1.B + c2.B + c3.B) / 3);


                    Color cBlured = Color.FromArgb(bR, bG, bB);

                    result.SetPixel(i, j, cBlured);

                }
            return result;
        }

        public override string ToString()
        {
            return "Атака размытием";
        }
    }
}
