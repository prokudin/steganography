﻿using System.Drawing;


namespace Steganography.Attack
{
    class BrightnessAttack : IAttackMethod
    {
        public Bitmap Attack(Bitmap image)
        {
            Bitmap result = new Bitmap(image.Width,image.Height);
           
            float factor = 1.05f;
            int width = image.Width;
            int height = image.Height;
            float R, G, B;
            Color current;
            for(int i=0,j=0;i< width;i++)
            {
                for (j=0;j<height;j++)
                {
                    current = image.GetPixel(i, j);

                    R = (factor * current.R);
                    G = (factor * current.G);
                    B = (factor * current.B);

                    result.SetPixel(i, j, Color.FromArgb(R>255?255:(byte)R, G > 255 ? 255 : (byte)G, B > 255 ? 255 : (byte)B));
                }
            }

            return result;
        }

        public override string ToString()
        {
            return "Атака яркостью";
        }
    }
}
