﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Steganography.Attack
{
    /// <summary>
    /// Интерфейс, описывающий функционал метода атаки
    /// </summary>
    public interface IAttackMethod
    {
        /// <summary>
        /// Атаковать изображение
        /// </summary>
        /// <param name="image">Изображение</param>
        /// <returns>Изображение после атаки</returns>
        Bitmap Attack(Bitmap image);
    }
}
