﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Steganography
{
    class LSBMethod2 : IStenographyMethod
    {

    #region Конструктор
        public LSBMethod2(): this(null)
        {
        }
        public LSBMethod2(Bitmap bitmap)
        {
            image = bitmap;

            EncryptMethod.Add(typeof(string), EncryptString);
            EncryptMethod.Add(typeof(Bitmap), EncryptImage);

            DecryptMethod.Add(typeof(string), DecryptString);
            DecryptMethod.Add(typeof(Bitmap), DecryptImage);
        }        
    #endregion

    #region Шифровка        
        public void Encrypt<T>(T o)
        {
            try
            {
                EncryptMethod[typeof(T)](o);
            }
            catch (Exception ex)
            {
                throw new Exception("Невозможно зашифровать объект. " + ex.Message);
            }
        }
        protected void EncryptString(object o)
        {
            if (!(o is string))
            {// если на вход подана не строка
                throw new ArgumentException($"Illegal Argument, Expected : string , Current : {o.GetType().ToString()}");
            }

            string str = o as string;

            if (str.Length > (image.Width * image.Height))
            {// если длина строки больше количества пикселей
                throw new ArgumentException($"String length must be <= {image.Width * image.Height - 5}, Currect length == {str.Length}");
            }

            //закодировать текст
            int size = str.Length;
            char[] chr = new char[1];
            byte[] symbol;
            int index = 0;

            for (int i = 0, height = image.Height, width = image.Width; i < height && index < size; i++)
            {
                for (int j = 0; j < width && index < size; j++, index++)
                {
                    chr[0] = str[index];
                    symbol = Encoding.GetEncoding(1251).GetBytes(chr);
                    SetPixelInfo(i, j, symbol[0]);
                }
            }
        }
        protected void EncryptImage(object o)
        {
            if (!(o is Bitmap))
            {// если на вход подана не строка
                throw new ArgumentException($"Illegal Argument, Expected : bitmap , Current : {o.GetType().ToString()}");
            }

            Bitmap newimage = o as Bitmap;

            if (newimage.Height > image.Height || newimage.Width > image.Width / 3 )
            {// если изображение слишком большое
                throw new ArgumentException($"Too big image, Max size : w={image.Width / 3} h= {image.Height} , Currect size : w={newimage.Width} h= {newimage.Height} ");
            }

            int index;
            Color current;
            for (int i = 0, height = newimage.Height, width = newimage.Width; i < height; i++)
            {
                index = -1;

                for (int j = 0; j < width; j++)
                {
                    current = newimage.GetPixel(j,i);

                    SetPixelInfo(i, ++index, current.R);
                    SetPixelInfo(i, ++index, current.G);
                    SetPixelInfo(i, ++index, current.B);
                }
            }
        }
        private delegate void EncryptObject(object o);
        private Dictionary<Type, EncryptObject> EncryptMethod = new Dictionary<Type, EncryptObject>();
    #endregion

    #region Расшифровка
        public T Decrypt<T>()
        {
            T result = default(T);

            try
            {
                result = (T)DecryptMethod[typeof(T)]();
            }
            catch (Exception ex)
            {
                throw new Exception("Невозможно расшифровать объект. " + ex.Message);
            }

            return result;
        }
        protected object DecryptString()
        {
            // определить длину текста
            int width = image.Width;
            int height = image.Height;
            int size = width * height;

            char[] result = new char[size];
            char[] charAray;
            int passedCharsCount = 0;
            
            for (int i = 0; i < height && passedCharsCount < size; i++)
            {
                for (int j = 0; j < width && passedCharsCount < size; j++, passedCharsCount++)
                {
                    charAray = Encoding.GetEncoding(1251).GetChars(new byte[] { GetPixelInfo(i, j) });
                    result[passedCharsCount] = charAray[0];
                }
            }

            return new string(result);
        }
        protected object DecryptImage()
        {
            int width = image.Width/3;
            int height = image.Height;

            Bitmap result = new Bitmap(width, height);

            int index;            
            for (int i = 0; i < height; i++)
            {
                index = -1;

                for (int j = 0; j < width; j++)
                {
                    result.SetPixel(j, i, Color.FromArgb(GetPixelInfo(i, ++index), GetPixelInfo(i, ++index), GetPixelInfo(i, ++index)));
                }
            }

            return result;
        }
        private delegate object DecryptObject();
        private Dictionary<Type, DecryptObject> DecryptMethod = new Dictionary<Type, DecryptObject>();
        #endregion

        protected byte GetPixelInfo(int row, int column)
        {
            Color color = image.GetPixel(column, row);
            BitArray colorArray = BitOperation.ByteToBit(color.R); //получаем байт цвета и преобразуем в массив бит
            BitArray messageArray = BitOperation.ByteToBit(color.R); ;//инициализируем результирующий массив бит
            messageArray[0] = colorArray[0];
            messageArray[1] = colorArray[1];

            colorArray = BitOperation.ByteToBit(color.G);//получаем байт цвета и преобразуем в массив бит
            messageArray[2] = colorArray[0];
            messageArray[3] = colorArray[1];
            messageArray[4] = colorArray[2];

            colorArray = BitOperation.ByteToBit(color.B);//получаем байт цвета и преобразуем в массив бит
            messageArray[5] = colorArray[0];
            messageArray[6] = colorArray[1];
            messageArray[7] = colorArray[2];

            return BitOperation.BitsToByte(ref messageArray);
        }
        protected void SetPixelInfo(int row, int column, byte info)
        {
            Color pColor = image.GetPixel(column, row);
            BitArray oldBits = BitOperation.ByteToBit(pColor.R);
            BitArray newBits = BitOperation.ByteToBit(info);

            oldBits[0] = newBits[0];
            oldBits[1] = newBits[1];

            byte newR = BitOperation.BitsToByte(ref oldBits);

            oldBits = BitOperation.ByteToBit(pColor.G);
            oldBits[0] = newBits[2];
            oldBits[1] = newBits[3];
            oldBits[2] = newBits[4];

            byte newG = BitOperation.BitsToByte(ref oldBits);

            oldBits = BitOperation.ByteToBit(pColor.B);
            oldBits[0] = newBits[5];
            oldBits[1] = newBits[6];
            oldBits[2] = newBits[7];

            byte newB = BitOperation.BitsToByte(ref oldBits);

            Color nColor = Color.FromArgb(newR, newG, newB); //новый цвет из полученных битов
            image.SetPixel(column, row, nColor);
        }


        private Bitmap image;
        public Bitmap Image { get => image; set => image = value; }
            
        public override string ToString()
        {
            return "LSB2";
        }

        public void Clear()
        {
            for (int i = 0, height = image.Height, width = image.Width; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    SetPixelInfo(i, j, 0);
                }
            }
        }
    }
}
