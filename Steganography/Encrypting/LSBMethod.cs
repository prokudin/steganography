﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Text;


namespace Steganography
{
    class LSBMethod : IStenographyMethod
    {

    #region Конструктор
        public LSBMethod()
            : this(null)
        {
        }
        public LSBMethod(Bitmap bitmap)
        {
            image = bitmap;

            EncryptMethod.Add(typeof(string), EncryptString);
            EncryptMethod.Add(typeof(Bitmap), EncryptImage);

            DecryptMethod.Add(typeof(string), DecryptString);
            DecryptMethod.Add(typeof(Bitmap), DecryptImage);
        }
        #endregion

    #region Шифровка

        public void Encrypt<T>(T o)
        {
            try
            {
                EncryptMethod[typeof(T)](o);
            }
            catch (Exception ex)
            {
                throw new Exception("Невозможно зашифровать объект. " + ex.Message);
            }
        }
        protected void EncryptString(object o)
        {
            if (!(o is string))
            {// если на вход подана не строка
                throw new ArgumentException($"Illegal Argument, Expected : string , Current : {o.GetType().ToString()}");
            }

            string str = o as string;

            if (str.Length > (image.Width * image.Height - 5))
            {// если длина строки больше количества пикселей
                throw new ArgumentException($"String length must be <= {image.Width * image.Height - 5}, Currect length == {str.Length}");
            }

            // закодировать флаг\
            byte[] symbol = Encoding.GetEncoding(1251).GetBytes("/");
            SetPixelInfo(0, 0, symbol[0]);

            // закодировать количество элементов
            byte[] bSize = BitConverter.GetBytes(str.Length);      // перевод int в массив байт
            for (int i = 0; i < bSize.Length; i++)
            {
                SetPixelInfo(0, i + 1, bSize[i]);
            }


            //закодировать текст
            int size = str.Length;
            char[] chr = new char[1];
            int index = 0;
            for (int i = 5, width = image.Width; i < width && index < size; i++, index++)
            {
                chr[0] = str[index];
                symbol = Encoding.GetEncoding(1251).GetBytes(chr);
                SetPixelInfo(0, i, symbol[0]);
            }

            for (int i = 1, height = image.Height, width = image.Width; i < height && index < size; i++)
            {
                for (int j = 0; j < width && index < size; j++, index++)
                {
                    chr[0] = str[index];
                    symbol = Encoding.GetEncoding(1251).GetBytes(chr);
                    SetPixelInfo(i, j, symbol[0]);
                }
            }
        }
        protected void EncryptImage(object o)
        {
            throw new NotImplementedException("Шифрование изображений не поддерживается методом");
        }
        private delegate void EncryptObject(object o);
        private Dictionary<Type, EncryptObject> EncryptMethod = new Dictionary<Type, EncryptObject>();    

    #endregion

    #region Расшифровка

        public T Decrypt<T>()
        {
            if (!IsEncrypted)
                return default(T);

            T result = default(T);
            
            try
            {
                result = (T)DecryptMethod[typeof(T)]();
            }
            catch (Exception ex)
            {
                throw new Exception("Невозможно расшифровать объект. " + ex.Message);
            }

            return result;
        }
        protected object DecryptString()
        {
            // определить длину текста
            byte[] bSize = new byte[4];
            for (int i = 0; i < 4; i++)
                bSize[i] = GetPixelInfo(0, i + 1);
            int size = BitConverter.ToInt32(bSize, 0);

            char[] result = new char[size];
            byte[] byteArr = new byte[1];
            char[] charAray;
            int passedCharsCount = 0;
            int width = image.Width;
            int height = image.Height;
            for (int i = 5; i < width && passedCharsCount < size; i++, passedCharsCount++)
            {
                byteArr[0] = GetPixelInfo(0, i);
                charAray = Encoding.GetEncoding(1251).GetChars(byteArr);
                result[passedCharsCount] = charAray[0];
            }

            for (int i = 1; i < height && passedCharsCount < size; i++)
            {
                for (int j = 0; j < width && passedCharsCount < size; j++, passedCharsCount++)
                {
                    byteArr[0] = GetPixelInfo(i, j);
                    charAray = Encoding.GetEncoding(1251).GetChars(byteArr);
                    result[passedCharsCount] = charAray[0];
                }
            }

            return new string(result);
        }
        protected object DecryptImage()
        {            
            throw new NotImplementedException("Расшифровка изображений не поддерживается методом");
        }
        private delegate object DecryptObject();
        private Dictionary<Type, DecryptObject> DecryptMethod = new Dictionary<Type, DecryptObject>();

    #endregion

        public void Clear()
        {
            for (int i = 0, height = image.Height, width = image.Width; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    SetPixelInfo(i, j, 0);
                }
            }
        }

        public bool IsEncrypted
        {
            get
            {
                char[] result = Encoding.GetEncoding(1251).GetChars(new byte[] { GetPixelInfo(0, 0) });

                return result[0] == '/';
            }
        }

        protected byte GetPixelInfo(int row, int column)
        {
            Color color = image.GetPixel(column, row);
            BitArray colorArray = BitOperation.ByteToBit(color.R); //получаем байт цвета и преобразуем в массив бит
            BitArray messageArray = BitOperation.ByteToBit(color.R); ;//инициализируем результирующий массив бит
            messageArray[0] = colorArray[0];
            messageArray[1] = colorArray[1];

            colorArray = BitOperation.ByteToBit(color.G);//получаем байт цвета и преобразуем в массив бит
            messageArray[2] = colorArray[0];
            messageArray[3] = colorArray[1];
            messageArray[4] = colorArray[2];

            colorArray = BitOperation.ByteToBit(color.B);//получаем байт цвета и преобразуем в массив бит
            messageArray[5] = colorArray[0];
            messageArray[6] = colorArray[1];
            messageArray[7] = colorArray[2];

            return BitOperation.BitsToByte(ref messageArray);
        }

        protected void SetPixelInfo(int row,int column, byte info)
        {
            
            Color pColor = image.GetPixel(column, row);
            BitArray oldBits = BitOperation.ByteToBit(pColor.R);
            BitArray newBits = BitOperation.ByteToBit(info);

            oldBits[0] = newBits[0];
            oldBits[1] = newBits[1];

            byte newR = BitOperation.BitsToByte(ref oldBits);

            oldBits = BitOperation.ByteToBit(pColor.G);
            oldBits[0] = newBits[2];
            oldBits[1] = newBits[3];
            oldBits[2] = newBits[4];

            byte newG = BitOperation.BitsToByte(ref oldBits);

            oldBits = BitOperation.ByteToBit(pColor.B);
            oldBits[0] = newBits[5];
            oldBits[1] = newBits[6];
            oldBits[2] = newBits[7];

            byte newB = BitOperation.BitsToByte(ref oldBits);

            Color nColor = Color.FromArgb(newR, newG, newB); //новый цвет из полученных битов
            image.SetPixel(column, row, nColor);
        }

        private Bitmap image;
        public Bitmap Image
        {
            get
            {
                return image;
            }

            set
            {
                image = value;
            }
        }
        

        public override string ToString()
        {
            return "LSB";
        }
    }
}
