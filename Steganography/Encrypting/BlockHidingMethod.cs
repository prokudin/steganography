﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Steganography
{
    class BlockHidingMethod : IStenographyMethod
    {

        public BlockHidingMethod()
        {
            EncryptMethod.Add(typeof(string), EncryptString);
            EncryptMethod.Add(typeof(Bitmap), EncryptImage);

            DecryptMethod.Add(typeof(string), DecryptString);
            DecryptMethod.Add(typeof(Bitmap), DecryptImage);
        }


        private Bitmap image;
        public Bitmap Image { get => image; set => image = value; }

        #region Шифровка            
        public void Encrypt<T>(T o)
        {
            try
            {
                EncryptMethod[typeof(T)](o);
            }
            catch (Exception ex)
            {
                throw new Exception("Невозможно зашифровать объект. " + ex.Message);
            }
        }
        protected void EncryptString(object o)
        {
            if (!(o is string))
            {// если на вход подана не строка
                throw new ArgumentException($"Illegal Argument, Expected : string , Current : {o.GetType().ToString()}");
            }

            string input = o as string;

            if (input.Length > (image.Width * image.Height - 5) * 4)
            {// если длина строки больше количества пикселей
                throw new ArgumentException("Too big string")/*($"String length must be <= {image.Width * image.Height-5}, Currect length == {input.Length}")*/;
            }

            // закодировать флаг\
            byte[] symbol = Encoding.GetEncoding(1251).GetBytes("/");
            SetPixelInfo(0, 0, symbol[0]);

            // закодировать количество элементов
            byte[] bSize = BitConverter.GetBytes(input.Length);      // перевод int в массив байт
            for (int i = 0; i < bSize.Length; i++)
            {
                SetPixelInfo(0, i + 1, bSize[i]);
            }


            //закодировать текст
            int size = input.Length;
            char[] chr = new char[1];
            int index = 0;
            for (int i = 5, width = image.Width; i < width && index < size; i++)
            {
                chr[0] = input[index];
                symbol = Encoding.GetEncoding(1251).GetBytes(chr);
                SetPixelInfo(0, i, symbol[0]);
                index++;
            }
            index = 0;
            for (int i = 1, height = image.Height, width = image.Width; i < height - 4 && index < size; i = i + 4)
            {
                for (int j = 0; j < width - 4 && index < size; j = j + 4)
                {
                    chr[0] = input[index];
                    symbol = Encoding.GetEncoding(1251).GetBytes(chr);
                    for (int k = i; k < image.Height && k < i + 4; k++)
                    {
                        for (int f = j; f < image.Width && f < j + 4; f++)
                        {
                            SetPixelInfo(k, f, symbol[0]);
                        }

                    }
                    index++;
                }
            }
        }
        protected void EncryptImage(object o)
        {
            if (!(o is Bitmap))
            {// если на вход подана не строка
                throw new ArgumentException("Illegal Argument, Expected : Bitmap ");
            }

            Bitmap input = o as Bitmap;

            if (input.Width > image.Width * 4 || input.Height > (image.Height / 3 - 1) * 4)
            {// если картинка, которую следует зашифровать большая
                throw new ArgumentException("Too big pic");
            }

            // закодировать флаг\
            byte[] symbol = Encoding.GetEncoding(1251).GetBytes("/");
            SetPixelInfo(0, 0, symbol[0]);

            //// закодировать количество элементов
            byte[] bSize1 = BitConverter.GetBytes(input.Height);      // перевод int в массив байт
            for (int i = 0; i < bSize1.Length; i++)
            {
                SetPixelInfo(0, i + 1, bSize1[i]);
            }
            byte[] bSize2 = BitConverter.GetBytes(input.Width);      // перевод int в массив байт
            for (int i = 0; i < bSize2.Length; i++)
            {
                SetPixelInfo(0, i + bSize1.Length + 1, bSize2[i]);
            }

            //закодировать 
            int heightPic = input.Height;
            int widthPic = input.Width;

            char[] chr = new char[1];
            int index = 0;
            int index2 = 0;
            // Для каждых 3х строк начиная со второй
            for (int i = 1, height = image.Height, width = image.Width; index < input.Height & i < height; i = i + 12)
            {
                index2 = 0;
                // Для каждого столбца
                for (int j = 0; j < width - 4 && index2 < input.Width; j = j + 4)
                {
                    Color color = input.GetPixel(index2, index);
                    for (int k = i; k < image.Height - 3 && k < i + 12; k += 3)
                    {
                        for (int f = j; f < image.Width - 3 && f < j + 4; f += 1)
                        {
                            // Зашифровать пиксель картинки, каждый цвет на отдельной строке
                            symbol = BitConverter.GetBytes(color.R);
                            SetPixelInfo(k, f, symbol[0]);
                            symbol = BitConverter.GetBytes(color.G);
                            SetPixelInfo(k + 1, f, symbol[0]);
                            symbol = BitConverter.GetBytes(color.B);
                            SetPixelInfo(k + 2, f, symbol[0]);
                        }
                    }
                    index2++;
                }

                index++;
            }
        }
        private delegate void EncryptObject(object o);
        private Dictionary<Type, EncryptObject> EncryptMethod = new Dictionary<Type, EncryptObject>();
        #endregion

        #region Расшифровка
        public T Decrypt<T>()
        {
            T result = default(T);

            try
            {
                result = (T)DecryptMethod[typeof(T)]();
            }
            catch (Exception ex)
            {
                throw new Exception("Невозможно расшифровать объект. " + ex.Message);
            }

            return result;
        }
        protected object DecryptString()
        {
            // определить длину текста
            byte[] bSize = new byte[4];
            for (int i = 0; i < 4; i++)
                bSize[i] = GetPixelInfo(0, i + 1);
            int size = BitConverter.ToInt32(bSize, 0);


            char[] result = new char[size];
            char[] charAray;
            int passedCharsCount = 0;
            int width = image.Width;
            int height = image.Height;
            for (int i = 5; i < width && passedCharsCount < size; i++)
            {
                charAray = Encoding.GetEncoding(1251).GetChars(new byte[] { GetPixelInfo(0, i) });
                result[passedCharsCount++] = charAray[0];
            }

            for (int i = 1; i < height && passedCharsCount < size; i = i + 4)
            {
                for (int j = 0; j < width && passedCharsCount < size; j = j + 4)
                {
                    charAray = Encoding.GetEncoding(1251).GetChars(new byte[] { GetPixelInfo(i, j) });
                    result[passedCharsCount++] = charAray[0];
                }
            }

            return new string(result);
        }
        protected object DecryptImage()
        {
            // определить длину картинки
            byte[] bSize = new byte[4];
            for (int i = 0; i < 4; i++)
                bSize[i] = GetPixelInfo(0, i + 1);
            int heightPic = BitConverter.ToInt32(bSize, 0);

            // Определить ширину картинки
            bSize = new byte[4];
            for (int i = 0; i < 4; i++)
                bSize[i] = GetPixelInfo(0, i + 5);
            int widthPic = BitConverter.ToInt32(bSize, 0);

            Bitmap encryptPic = new Bitmap(widthPic, heightPic);
            int width = image.Width;
            int height = image.Height;

            int index = 0;
            int R = 0;
            int G = 0;
            int B = 0;
            int index2;
            for (int i = 1; i < height - 12 && index < heightPic; i = i + 12)
            {
                index2 = 0;
                for (int j = 0; j < width && index2 < widthPic; j = j + 4)
                {
                    R = Convert.ToInt32(GetPixelInfo(i, j));
                    G = Convert.ToInt32(GetPixelInfo(i + 1, j));
                    B = Convert.ToInt32(GetPixelInfo(i + 2, j));
                    Color color = Color.FromArgb(R, G, B);
                    encryptPic.SetPixel(index2, index, color);
                    index2++;
                }
                index++;
            }

            return new Bitmap(encryptPic);
        }
        private delegate object DecryptObject();
        private Dictionary<Type, DecryptObject> DecryptMethod = new Dictionary<Type, DecryptObject>();
        #endregion

        protected byte GetPixelInfo(int row, int column)
        {
            Color color = image.GetPixel(column, row);
            BitArray colorArray = BitOperation.ByteToBit(color.R); //получаем байт цвета и преобразуем в массив бит
            BitArray messageArray = BitOperation.ByteToBit(color.R); ;//инициализируем результирующий массив бит
            messageArray[0] = colorArray[0];
            messageArray[1] = colorArray[1];

            colorArray = BitOperation.ByteToBit(color.G);//получаем байт цвета и преобразуем в массив бит
            messageArray[2] = colorArray[0];
            messageArray[3] = colorArray[1];
            messageArray[4] = colorArray[2];

            colorArray = BitOperation.ByteToBit(color.B);//получаем байт цвета и преобразуем в массив бит
            messageArray[5] = colorArray[0];
            messageArray[6] = colorArray[1];
            messageArray[7] = colorArray[2];

            return BitOperation.BitsToByte(ref messageArray);
        }
        protected void SetPixelInfo(int row, int column, byte info)
        {
            Color pColor = image.GetPixel(column, row);
            BitArray oldBits = BitOperation.ByteToBit(pColor.R);
            BitArray newBits = BitOperation.ByteToBit(info);

            oldBits[0] = newBits[0];
            oldBits[1] = newBits[1];

            byte newR = BitOperation.BitsToByte(ref oldBits);

            oldBits = BitOperation.ByteToBit(pColor.G);
            oldBits[0] = newBits[2];
            oldBits[1] = newBits[3];
            oldBits[2] = newBits[4];

            byte newG = BitOperation.BitsToByte(ref oldBits);

            oldBits = BitOperation.ByteToBit(pColor.B);
            oldBits[0] = newBits[5];
            oldBits[1] = newBits[6];
            oldBits[2] = newBits[7];

            byte newB = BitOperation.BitsToByte(ref oldBits);

            Color nColor = Color.FromArgb(newR, newG, newB); //новый цвет из полученных битов
            image.SetPixel(column, row, nColor);
        }
        
        public override string ToString()
        {
            return "Скрытие Блоков";
        }

        public void Clear()
        {
            for (int i = 0, height = image.Height, width = image.Width; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    SetPixelInfo(i, j, 0);
                }
            }
        }
    }
}

