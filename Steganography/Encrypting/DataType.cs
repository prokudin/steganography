﻿namespace Steganography
{
    public enum DataType
    {
        TEXT = 0,
        IMAGE = 1
    }
}
