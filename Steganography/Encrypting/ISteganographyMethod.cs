﻿using System.Drawing;


namespace Steganography
{
    /// <summary>
    /// Интерфейс, описывающий метод шифрования
    /// </summary>
    public interface IStenographyMethod
    {
        /// <summary>
        /// Зашифровать объект в изображении
        /// </summary>
        /// <typeparam name="T">Тип входного объекта</typeparam>
        /// <param name="o">Шифруемый объект</param>
        void Encrypt<T>(T o);

        /// <summary>
        /// Расшифровать объект
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <returns></returns>
        T Decrypt<T>();

        /// <summary>
        /// Очистить следы шифрования
        /// </summary>
        void Clear();

        /// <summary>
        /// Изображение
        /// </summary>
        Bitmap Image { get; set; }
    }
}
