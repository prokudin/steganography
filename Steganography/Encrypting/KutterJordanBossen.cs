﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

using System.Threading.Tasks;

namespace Steganography
{
    class KutterJordanBossen : IStenographyMethod
    {
    #region Конструктор
        public KutterJordanBossen()
        {
            EncryptMethod.Add(typeof(Bitmap), EncryptImage);
            EncryptMethod.Add(typeof(string), EncryptString);

            DecryptMethod.Add(typeof(Bitmap), DecryptImage);
            DecryptMethod.Add(typeof(string), DecryptString);
        }
    #endregion

        public Bitmap Image { get => image; set => image=value; }
        private Bitmap image;


        // параметры для анализа
        public static float Lambda = 0.1f;
        public static int Sigma = 2;


        private Color[,] ColorMap = null;

    #region Дешифровка

        public T Decrypt<T>()
        {
            T result = default(T);

            try
            {
                FillColorMap();
                result = (T)DecryptMethod[typeof(T)]();
            }
            catch (Exception ex)
            {
                throw new Exception("Невозможно расшифровать объект. " + ex.Message);
            }

            return result;
        }

        private void FillColorMap()
        {
            ColorMap = new Color[image.Height, image.Width];

            for (int i=0,height = image.Height, width = image.Width; i< height; i++)
            {
                for (int j = 0;j<width;j++)
                {
                    ColorMap[i, j] = image.GetPixel(j, i);
                }
            }
        }

        private object DecryptString()
        {
            BitArray bitStr = Decrypt();

            int size = bitStr.Length / 8;

            char[] result;
            byte[] resultByte = new byte[size];
            
            for (int i=0;i< size; i++)
            {
                resultByte[i] = BitOperation.BitsToByte(ref bitStr, i * 8, i * 8 + 7);
            }

            result = Encoding.GetEncoding(1251).GetChars(resultByte);

            return new string(result);
        }

        private object DecryptImage()
        { 
            int resultHeight = (image.Height - Sigma * 2) / 5;
            int resultWidth = (image.Width - Sigma * 2) / 5;

            Bitmap result = new Bitmap(resultWidth, resultHeight);

            
            for (int i = 0; i < resultWidth; i++)
            {
                for (int j = 0; j < resultHeight; j++)
                {
                    int minX = i * 5 + Sigma;
                    int minY = j * 5 + Sigma;

                    Color currentPixel = image.GetPixel(i, j);

                    BitArray bits = new BitArray(24);
                    int index = 0;

                    for (int x = minX, size = 24; x < minX + 5 && index < size; x++)
                    {
                        for (int y = minY; y < minY + 5 && index < size; y++, index++)
                        {
                            bits[index] = GetBit(x, y, ref image);
                        }
                    }

                    result.SetPixel(i, j, Color.FromArgb(BitOperation.BitsToByte(ref bits, 0, 7), BitOperation.BitsToByte(ref bits, 8, 15), BitOperation.BitsToByte(ref bits, 16, 23)));
                }
            }

            return result;
        }

        private bool GetBit(int x,int y, ref Bitmap image)
        {
            Color current = ColorMap[y, x];

            int sum = 0;
            for (int k = 1; k <= Sigma; k++)
            {                
                sum += ColorMap[y,x + k].B;                
                sum += ColorMap[y,x - k].B;                
                sum += ColorMap[y + k,x].B;                
                sum += ColorMap[y - k,x].B;
            }

            return current.B > ((float)sum) / (4 * Sigma);
        }

        private BitArray Decrypt()
        {
            BitArray result = new BitArray((image.Width-2 * Sigma) * (image.Height - 2 * Sigma));

            int index = 0;

            for (int i = Sigma , height = image.Height - Sigma, width = image.Width - Sigma; i < width; i++)
            {
                for (int j = Sigma ; j < height; j++,index++)
                {
                    result[index] = GetBit(i,j,ref image);
                }
            }

            return result;
        }
        private delegate object DecryptObject();
        private Dictionary<Type, DecryptObject> DecryptMethod = new Dictionary<Type, DecryptObject>();
        
        
    #endregion

    #region Шифровка

        public void Encrypt<T>(T o)
        {
            try
            {
                EncryptMethod[typeof(T)](o);
            }
            catch (Exception ex)
            {
                throw new Exception("Невозможно зашифровать объект. " + ex.Message);
            }
        }

        private void EncryptString(object o)
        {
            if (!(o is string))
                throw new Exception($"Некорретный тип. Требуется строка. Текущий тип : {o.GetType()}");

            BitArray str = new BitArray(Encoding.GetEncoding(1251).GetBytes(o as string));            

            if (str.Length > (image.Height - 2 * Sigma) * (image.Width - 2 * Sigma))
                throw new Exception($"Слишком большая длина строки. Текущая длина : {str.Length}. Максимальная длина : {image.Height * image.Width}");

            Encrypt(ref str);
        }

        private void EncryptImage(object o)
        {           

            if (!(o is Bitmap))
            {
                throw new Exception($"Некорретный тип. Требуется картинка(Bitmap). Текущий тип : {o.GetType()}");
            }

            Bitmap newImage = o as Bitmap;

            int maxHeight = (image.Height - Sigma * 2) / 5;
            int maxWidth = (image.Width - Sigma * 2) / 5;

            if (maxHeight < newImage.Height || maxWidth < newImage.Width)
            {
                throw new Exception($"Слишком большой размер шифруемой картинки. Текущий размер : {newImage.Height} * {newImage.Width}. Максимальный размер : {maxHeight} * {maxWidth}");
            }
            
            for (int i = 0, height = newImage.Height, width = newImage.Width; i < width; i++)
            {
                for (int j=0;j < height; j++)
                {
                    int minX = i * 5 + Sigma;
                    int minY = j * 5 + Sigma;

                    Color currentPixel = newImage.GetPixel(i, j);

                    BitArray bits = new BitArray(new byte[] { currentPixel.R, currentPixel.G, currentPixel.B });

                    int index = 0;                    
                    for (int x= minX, size = bits.Length ; x < minX + 5  && index < size; x++ )
                    {
                        for (int y = minY; y < minY + 5 && index < size; y++, index++)
                        {
                            SetBit(x, y, ref image, bits[index]);
                        }
                    }
                }
            }
        }

        private void SetBit(int x, int y, ref Bitmap image,bool bit)
        {
            Color current = image.GetPixel(x, y);

            float f = 0.3f * current.R + 0.59f * current.G + 0.11f * current.B;

            int newB = bit ? (int)(current.B + Lambda * f) : (int)(current.B - Lambda * f);

            if (newB > 255)
                newB = 255;

            if (newB < 0)
                newB = 0;

            image.SetPixel(x, y, Color.FromArgb(current.R, current.G, newB));
        }

        private void Encrypt(ref BitArray info)
        {
            int index = 0;
            
            for (int i = Sigma, height = image.Height- Sigma, width = image.Width- Sigma, size = info.Length; i < width && index < size; i++)
            {
                for (int j = Sigma; j < height && index < size; j++,index++)
                {
                    SetBit(i, j, ref image, info[index]);
                }
            }
        }


        private delegate void EncryptObject(object o);
        private Dictionary<Type, EncryptObject> EncryptMethod = new Dictionary<Type, EncryptObject>();
        #endregion

        public override string ToString()
        {
            return "KJB";
        }

        public void Clear()
        {
            Random rnd = new Random();

            for (int i = Sigma, height = image.Height - Sigma, width = image.Width - Sigma; i < width; i++)
            {
                for (int j = Sigma; j < height ; j++)
                {
                    SetBit(i, j, ref image, rnd.Next(0,2)==1);
                }
            }
        }
    }
}
