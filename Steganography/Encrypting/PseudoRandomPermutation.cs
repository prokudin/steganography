﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Collections;

namespace Steganography
{
    class PseudoRandomPermutation : IStenographyMethod
    {
        public PseudoRandomPermutation()
        {
            EncryptMethod.Add(typeof(string), EncryptString);
            EncryptMethod.Add(typeof(Bitmap), EncryptImage);

            DecryptMethod.Add(typeof(string), DecryptString);
            DecryptMethod.Add(typeof(Bitmap), DecryptImage);
        }


        private Bitmap image;
        public Bitmap Image { get => image; set => image = value; }

        private Random randomGenerator = null;

        LinkedList<Tuple<int, int>> generateIndexes(int messageLength, int imageHeight, int imageWidth)
        {
            LinkedList<Tuple<int, int>> indexes = new LinkedList<Tuple<int, int>>();
            randomGenerator = new Random(100);
            for (int i = 0; i < messageLength; i++)
            {
                indexes.AddLast(new Tuple<int, int>(randomGenerator.Next(0, imageHeight), randomGenerator.Next(0, imageWidth)));
            }

            return indexes;
        }

        public void Clear()
        {
            for (int i = 0, height = image.Height, width = image.Width; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    SetPixelInfo(i, j, 0);
                }
            }
        }

        protected byte GetPixelInfo(int row, int column)
        {
            Color color = image.GetPixel(column, row);
            BitArray colorArray = BitOperation.ByteToBit(color.R); //получаем байт цвета и преобразуем в массив бит
            BitArray messageArray = BitOperation.ByteToBit(color.R); ;//инициализируем результирующий массив бит
            messageArray[0] = colorArray[0];
            messageArray[1] = colorArray[1];

            colorArray = BitOperation.ByteToBit(color.G);//получаем байт цвета и преобразуем в массив бит
            messageArray[2] = colorArray[0];
            messageArray[3] = colorArray[1];
            messageArray[4] = colorArray[2];

            colorArray = BitOperation.ByteToBit(color.B);//получаем байт цвета и преобразуем в массив бит
            messageArray[5] = colorArray[0];
            messageArray[6] = colorArray[1];
            messageArray[7] = colorArray[2];

            return BitOperation.BitsToByte(ref messageArray);
        }
        protected void SetPixelInfo(int row, int column, byte info)
        {
            Color pColor = image.GetPixel(column, row);
            BitArray oldBits = BitOperation.ByteToBit(pColor.R);
            BitArray newBits = BitOperation.ByteToBit(info);

            oldBits[0] = newBits[0];
            oldBits[1] = newBits[1];

            byte newR = BitOperation.BitsToByte(ref oldBits);

            oldBits = BitOperation.ByteToBit(pColor.G);
            oldBits[0] = newBits[2];
            oldBits[1] = newBits[3];
            oldBits[2] = newBits[4];

            byte newG = BitOperation.BitsToByte(ref oldBits);

            oldBits = BitOperation.ByteToBit(pColor.B);
            oldBits[0] = newBits[5];
            oldBits[1] = newBits[6];
            oldBits[2] = newBits[7];

            byte newB = BitOperation.BitsToByte(ref oldBits);

            Color nColor = Color.FromArgb(newR, newG, newB); //новый цвет из полученных битов
            image.SetPixel(column, row, nColor);
        }




        public T Decrypt<T>()
        {
            T result = default(T);

            try
            {
                result = (T)DecryptMethod[typeof(T)]();
            }
            catch (Exception ex)
            {
                throw new Exception("Невозможно расшифровать объект. " + ex.Message);
            }

            return result;
        }

        protected string DecryptString()
        {
            // определить длину текста
            int width = image.Width;
            int height = image.Height;
            int size = width * height;

            char[] result = new char[size];
            char[] charAray;

            // сгенерировать последовательность пикселей
            LinkedList<Tuple<int, int>> indexes = generateIndexes(size, image.Height, image.Width);
            int i = 0;
            foreach (Tuple<int, int> index in indexes)
            {
                charAray = Encoding.GetEncoding(1251).GetChars(new byte[] { GetPixelInfo(index.Item1, index.Item2) });
                result[i++] = charAray[0];
            }

            return new string(result);
        }
        protected Bitmap DecryptImage()
        {
            int width = 150;
            int height = 150;

            Bitmap result = new Bitmap(width, height);

            string s = DecryptString() as string;


            char[] cheArr = new char[150 * 150 * 3];
            byte R, G, B;
            int index = 0;
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {

                    R = (byte)s[index++];
                    G = (byte)s[index++];
                    B = (byte)s[index++];

                    result.SetPixel(j, i, Color.FromArgb(R, G, B));
                }
            }

            return result;
        }

        private delegate object DecryptObject();
        private Dictionary<Type, DecryptObject> DecryptMethod = new Dictionary<Type, DecryptObject>();



        private delegate void EncryptObject(object o);
        private Dictionary<Type, EncryptObject> EncryptMethod = new Dictionary<Type, EncryptObject>();

        public void Encrypt<T>(T o)
        {
            try
            {
                EncryptMethod[typeof(T)](o);
            }
            catch (Exception ex)
            {
                throw new Exception("Невозможно зашифровать объект. " + ex.Message);
            }
        }

        protected void EncryptString(object o)
        {
            if (!(o is string))
            {// если на вход подана не строка
                throw new ArgumentException($"Illegal Argument, Expected : string , Current : {o.GetType().ToString()}");
            }

            string str = o as string;

            if (str.Length > (image.Width * image.Height))
            {// если длина строки больше количества пикселей
                throw new ArgumentException($"String length must be <= {image.Width * image.Height - 5}, Currect length == {str.Length}");
            }

            //закодировать текст

            // сгенерировать последовательность пикселей
            LinkedList<Tuple<int, int>> indexes = generateIndexes(str.Length, image.Height, image.Width);
            char[] chr = new char[1];
            byte[] symbol;
            int i = 0, width = image.Width, height = image.Height;
            foreach (Tuple<int, int> index in indexes)
            {
                chr[0] = str[i++];
                symbol = Encoding.GetEncoding(1251).GetBytes(chr);
                SetPixelInfo(index.Item1, index.Item2, symbol[0]);
            }
        }

        public static Bitmap resizeImage(Bitmap imgToResize, Size size)
        {
            return (new Bitmap(imgToResize, size));
        }

        protected void EncryptImage(object o)
        {
            if (!(o is Bitmap))
            {// если на вход подана не строка
                throw new ArgumentException($"Illegal Argument, Expected : bitmap , Current : {o.GetType().ToString()}");
            }

            Bitmap newimage = o as Bitmap;

            if (newimage.Height > 150 || newimage.Width > 150)
            {// если изображение слишком большое
                throw new ArgumentException($"Too big image, Max size : w={image.Width / 3} h= {image.Height} , Currect size : w={newimage.Width} h= {newimage.Height} ");
            }

            newimage = resizeImage(newimage, new Size(150, 150));


            char[] cheArr = new char[150 * 150 * 3];
            Color current;
            int index = 0;
            for (int i = 0, height = newimage.Height, width = newimage.Width; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    current = newimage.GetPixel(j, i);
                    cheArr[index++] = (char)current.R;
                    cheArr[index++] = (char)current.G;
                    cheArr[index++] = (char)current.B;
                }
            }

            object s = new string(cheArr);

            EncryptString(s);
        }


        public override string ToString()
        {
            return "Псевдослучайная перестановка";
        }
    }
}