﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.Structure;

namespace Steganography
{
    class AbstractDigitalWatermarkingDWT : IStenographyMethod
    {
        
        private int ARNOLD_TRANSFORMS_COUNT = 50;

        public bool ResizeImageTo2nSize = false;

        #region Конструктор
        public AbstractDigitalWatermarkingDWT(bool resizeImageTo2nSize = false) : this(null, resizeImageTo2nSize)
        {
        }
        public AbstractDigitalWatermarkingDWT(Bitmap bitmap, bool resizeImageTo2nSize = false)
        {
            image = bitmap;
            ResizeImageTo2nSize = resizeImageTo2nSize;

            EncryptMethod.Add(typeof(string), EncryptString);
            EncryptMethod.Add(typeof(Bitmap), EncryptImage);

            DecryptMethod.Add(typeof(string), DecryptString);
            DecryptMethod.Add(typeof(Bitmap), DecryptImage);
        }
        #endregion

        #region Шифровка        
        public void Encrypt<T>(T o)
        {
            try
            {
                EncryptMethod[typeof(T)](o);
            }
            catch (Exception ex)
            {
                throw new Exception("Невозможно зашифровать объект. " + ex.Message);
            }
        }
        protected void EncryptString(object o)
        {
            throw new NotSupportedException("Supports only image encrypting/decrypting");
        }
        protected void EncryptImage(object o)
        {
            if (!(o is Bitmap))
            {// если на вход подана не строка
                throw new ArgumentException($"Illegal Argument, Expected : bitmap , Current : {o.GetType().ToString()}");
            }

            Bitmap newimage = o as Bitmap;

            // расширить размер исходного изображения до кратного степени 2ки
            if (ResizeImageTo2nSize)
                ResizeImageTo2N(ref image);
            height_2n = Math.Min(ChangeTo2N(image.Height), ChangeTo2N(image.Width));
            width_2n = height_2n;

            // обрезать входное изображение или расширить его
            int newImageHeight = height_2n / 8;
            int newImageWidth = width_2n / 8;
            newimage = new Bitmap(newimage, new Size(newImageWidth, newImageHeight));

            // Конвертировать исходное изображение в YCbCr            
            var yccImage = new Image<Bgr, byte>(image).Convert<Ycc, byte>();

            // извлечение Y-channel
            var YChannel = ExternChannel(yccImage.Data, height_2n, width_2n, 0);
            double[,] YChannelDouble = new double[height_2n, width_2n];
            Array.Copy(YChannel, YChannelDouble, YChannel.Length);

            // 3-way dwt
            HWT_2D(YChannelDouble, 3);

            // quantize values of micro image
            RoundMatrix(YChannelDouble, newImageHeight, newImageWidth);

            // Arnold Transform for encrypting Image
            newimage = FastArnoldTransform(newimage, ARNOLD_TRANSFORMS_COUNT);

            // получить крту изображения в градациях серого
            bool[,] newimageBW = FromRgbToBlackWhite(newimage);

            // внедрение в последний значащий бит Y            
            for (int i = 0, heigth = newimage.Height, width = newimage.Width; i < heigth; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    YChannelDouble[i, j] = SetBit((uint)YChannelDouble[i, j], newimageBW[i, j]);
                }
            }

            // inverse 3-way dwt for Y-channel
            IHWT_2D(YChannelDouble, 3);

            // quantize Y-channel and converting to byte format 
            RoundMatrix(YChannelDouble);
            for (int i = 0; i < height_2n; i++)
            {
                for (int j = 0; j < width_2n; j++)
                {
                    YChannel[i, j] = Scale(YChannelDouble[i, j]);
                }
            }


            var data = SetChannelToData(yccImage.Data, YChannel, height_2n, width_2n, 0);
            yccImage = new Image<Ycc, byte>(data);
            image = yccImage.Convert<Bgr, byte>().ToBitmap();
        }
        private delegate void EncryptObject(object o);
        private Dictionary<Type, EncryptObject> EncryptMethod = new Dictionary<Type, EncryptObject>();

        #endregion

        #region Расшифровка
        public T Decrypt<T>()
        {
            T result = default(T);

            try
            {
                result = (T)DecryptMethod[typeof(T)]();
            }
            catch (Exception ex)
            {
                throw new Exception("Невозможно расшифровать объект. " + ex.Message);
            }

            return result;
        }
        protected object DecryptString()
        {
            throw new NotSupportedException("Supports only image encrypting/decrypting");
        }
        protected object DecryptImage()
        {
            // расширить размер исходного изображения до кратного степени 2ки
            if (ResizeImageTo2nSize)
                ResizeImageTo2N(ref image);
            height_2n = Math.Min(ChangeTo2N(image.Height), ChangeTo2N(image.Width));
            width_2n = height_2n;

            int width = width_2n / 8;
            int height = height_2n / 8;

            // Конвертировать исходное изображение в YCbCr            
            var yccImage = new Image<Bgr, byte>(image).Convert<Ycc, byte>();

            // extern Y-channel
            var YChannel = ExternChannel(yccImage.Data, height_2n, width_2n, 0);
            double[,] YChannelDouble = new double[height_2n, width_2n];
            Array.Copy(YChannel, YChannelDouble, YChannel.Length);

            // 3-way dwt
            HWT_2D(YChannelDouble, 3);

            // quantize values of micro image
            RoundMatrix(YChannelDouble, height, width);

            // extern bits
            bool[,] bitMap = new bool[height, width];
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    bitMap[i, j] = GetBit((uint)YChannelDouble[i, j]);
                }
            }

            var result = FromBWToBitmap(bitMap);

            // AntiArnold Transform
            result = FastAntiArnoldTransform(result, ARNOLD_TRANSFORMS_COUNT);

            return result;
        }
        private delegate object DecryptObject();
        private Dictionary<Type, DecryptObject> DecryptMethod = new Dictionary<Type, DecryptObject>();
        #endregion

        private byte[,,] SetChannelToData(byte[,,] data, byte[,] channel, int channelIndex)
        {
            return SetChannelToData(data, channel, channel.GetLength(0), channel.GetLength(1), channelIndex);
        }

        private byte[,,] SetChannelToData(byte[,,] data, byte[,] channel, int height, int width, int channelIndex)
        {
            byte[,,] result = data.Clone() as byte[,,];

            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    result[j, i, channelIndex] = channel[i, j];
                }
            }

            return result;
        }

        protected static Bitmap FromBWToBitmap(bool[,] matrix)
        {
            int height = matrix.GetLength(0);
            int width = matrix.GetLength(0);

            Bitmap result = new Bitmap(width, height);
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    result.SetPixel(j, i, matrix[i, j] ? Color.White : Color.Black);
                }
            }

            return result;
        }

        protected static byte Scale(double numb)
        {
            if (numb > 255)
                return 255;

            if (numb < 0)
                return 0;

            return (byte)numb;
        }

        protected static byte[,] ExternChannel(byte[,,] data, int height, int width, int channel)
        {
            byte[,] result = new byte[height, width];

            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    result[i, j] = data[j, i, channel];
                }
            }

            return result;
        }

        #region Diskrete Wavelet Transform
        protected static double DWT_k = 0.5;
        protected static void HWT_2D(double[,] data, int iterations)
        {
            int height = data.GetLength(0);
            int width = data.GetLength(1);
            for (int iter = 0; iter < iterations; iter++, width >>= 1, height >>= 1)
            {
                for (int i = 0; i < height; i++)
                {// для каждой строки
                    //clone string
                    double[] line = new double[width];
                    for (int j = 0; j < width; j++)
                        line[j] = data[i, j];

                    line = HWT_1D(line);

                    //apply
                    for (int j = 0; j < width; j++)
                        data[i, j] = line[j];
                }

                for (int i = 0; i < width; i++)
                {// для каждого столбца
                    //clone string
                    double[] line = new double[height];
                    for (int j = 0; j < height; j++)
                        line[j] = data[j, i];

                    line = HWT_1D(line);

                    //apply
                    for (int j = 0; j < width; j++)
                        data[j, i] = line[j];
                }
            }
        }
        protected static double[] HWT_1D(double[] data)
        {
            double[] result = new double[data.Length];
            int len = data.Length >> 1;
            for (int i = 0; i < len; i++)
            {
                result[i] = (data[2 * i] + data[2 * i + 1]) * DWT_k;
                result[i + len] = (data[2 * i] - data[2 * i + 1]) * DWT_k;
            }
            return result;
        }
        protected static void IHWT_2D(double[,] data, int iterations)
        {
            int height = data.GetLength(0) >> (iterations - 1);
            int width = data.GetLength(1) >> (iterations - 1);
            for (int iter = 0; iter < iterations; iter++, width <<= 1, height <<= 1)
            {
                for (int i = 0; i < width; i++)
                {// для каждого столбца
                    //clone string
                    double[] line = new double[height];
                    for (int j = 0; j < height; j++)
                        line[j] = data[j, i];

                    line = IHWT_1D(line);

                    //apply
                    for (int j = 0; j < width; j++)
                        data[j, i] = line[j];
                }

                for (int i = 0; i < height; i++)
                {// для каждой строки
                    //clone string
                    double[] line = new double[width];
                    for (int j = 0; j < width; j++)
                        line[j] = data[i, j];

                    line = IHWT_1D(line);

                    //apply
                    for (int j = 0; j < width; j++)
                        data[i, j] = line[j];
                }
            }
        }
        protected static double[] IHWT_1D(double[] data)
        {
            double[] result = new double[data.Length];
            int len = data.Length >> 1;
            for (int i = 0; i < len; i++)
            {
                result[2 * i] = (data[i] + data[i + len]) / (2 * DWT_k);
                result[2 * i + 1] = (data[i] - data[i + len]) / (2 * DWT_k);
            }
            return result;
        }
        #endregion

        protected static void RoundMatrix(double[,] data)
        {
            RoundMatrix(data, data.GetLength(0), data.GetLength(1));
        }
        protected static void RoundMatrix(double[,] data, int height, int width)
        {
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    data[i, j] = Math.Round(data[i, j]);
                }
            }
        }

        protected static Bitmap ArnoldTransform(Bitmap img)
        {
            Bitmap result = new Bitmap(img.Width, img.Height);

            for (int i = 0; i < img.Width; i++)
            {
                for (int j = 0; j < img.Height; j++)
                {
                    result.SetPixel((i + j) % img.Width, (i + 2 * j) % img.Height, img.GetPixel(i, j));
                }
            }

            return result;
        }
        protected static Bitmap ArnoldTransform(Bitmap img, int iterations)
        {
            Bitmap result = new Bitmap(img);
            for (var i = 0; i < iterations; i++)
            {
                result = ArnoldTransform(result);
            }
            return result;
        }
        protected static Bitmap FastArnoldTransform(Bitmap img, int iterations)
        {
            int width = img.Width;
            int height = img.Height;
            long[,] current = new long[img.Width, img.Height];
            long[,] prev = new long[img.Width, img.Height];

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    prev[i, j] = j + ((long)i << 32);
                }
            }

            current = (long[,])prev.Clone();
            int x, y;
            for (int c = 0; c < iterations; c++)
            {
                for (int i = 0; i < width; i++)
                {
                    for (int j = 0; j < height; j++)
                    {
                        x = (int)(prev[i, j] >> 32);
                        y = (int)(prev[i, j]);
                        current[i, j] = ((long)((x + y) % width) << 32) + (x + 2 * y) % height;
                    }
                }

                prev = (long[,])current.Clone();
            }

            Bitmap result = new Bitmap(width, height);
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    result.SetPixel((int)(current[i, j] >> 32), (int)(current[i, j]), img.GetPixel(i, j));
                }
            }

            return result;
        }

        protected static Bitmap AntiArnoldTransform(Bitmap img)
        {
            Bitmap result = new Bitmap(img.Width, img.Height);
            int x, y;
            for (int i = 0; i < img.Width; i++)
            {
                for (int j = 0; j < img.Height; j++)
                {
                    x = (2 * i - j) % img.Width;
                    y = (-i + j) % img.Height;
                    result.SetPixel(x >= 0 ? x : img.Width + x, y >= 0 ? y : img.Height + y, img.GetPixel(i, j));
                }
            }

            return result;
        }
        protected static Bitmap AntiArnoldTransform(Bitmap img, int iterations)
        {
            Bitmap result = new Bitmap(img);
            for (var i = 0; i < iterations; i++)
            {
                result = AntiArnoldTransform(result);
            }
            return result;
        }
        protected static Bitmap FastAntiArnoldTransform(Bitmap img, int iterations)
        {
            int width = img.Width;
            int height = img.Height;
            long[,] current = new long[img.Width, img.Height];
            long[,] prev = new long[img.Width, img.Height];

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    prev[i, j] = j + ((long)i << 32);
                }
            }

            current = (long[,])prev.Clone();
            int x, y, px, py;
            for (int c = 0; c < iterations; c++)
            {
                for (int i = 0; i < width; i++)
                {
                    for (int j = 0; j < height; j++)
                    {
                        px = (int)(prev[i, j] >> 32);
                        py = (int)(prev[i, j]);
                        x = (2 * px - py) % width;
                        y = (-px + py) % height;
                        x = x >= 0 ? x : width + x;
                        y = y >= 0 ? y : height + y;
                        current[i, j] = ((long)x << 32) + y;
                    }
                }

                prev = (long[,])current.Clone();
            }

            Bitmap result = new Bitmap(width, height);
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    result.SetPixel((int)(current[i, j] >> 32), (int)(current[i, j]), img.GetPixel(i, j));
                }
            }

            return result;
        }

        protected static bool[,] FromRgbToBlackWhite(Bitmap image, int intensity = 120)
        {
            intensity = (intensity > 255) ? 255 : intensity;
            intensity = (intensity < 0) ? 0 : intensity;

            bool[,] result = new bool[image.Height, image.Width];

            for (int i = 0; i < image.Height; i++)
            {
                for (int j = 0; j < image.Width; j++)
                {
                    Color c = image.GetPixel(j, i);
                    result[i, j] = ((c.R + c.G + c.B) / 3f >= intensity);
                }
            }

            return result;
        }

        protected static void ResizeImageTo2N(ref Bitmap image, int maxHeight = 0x10000000, int maxWidth = 0x10000000)
        {
            int newHeight = (image.Height > maxHeight) ? maxHeight : image.Height;
            int newWidth = (image.Width > maxWidth) ? maxWidth : image.Width;

            if ((newHeight & (newHeight - 1)) != 0)
            {
                int tmp = 1;
                while (tmp < newHeight)
                {
                    tmp = tmp << 1;
                }
                newHeight = tmp;
            }

            if ((newWidth & (newWidth - 1)) != 0)
            {
                int tmp = 1;
                while (tmp < newWidth)
                {
                    tmp = tmp << 1;
                }
                newWidth = tmp;
            }

            image = new Bitmap(image, new Size(newWidth, newHeight));
        }

        /// <summary>
        /// Возвращает число меньшее или равное value, являющееся степенью двойки
        /// </summary>
        protected static int ChangeTo2N(int value, int maxValue = 0x10000000)
        {
            int newValue = value > maxValue ? maxValue : value;

            if ((newValue & (newValue - 1)) != 0)
            {
                int tmp = 1;
                while (tmp < newValue)
                    tmp <<= 1;
                newValue = tmp >> 1;
            }

            return newValue;
        }

        protected byte EncryptBit = 0b00000001;
        protected uint SetBit(uint val, bool bit)
        {
            val = val & (~((uint)EncryptBit));
            return val | (uint)(bit ? EncryptBit : 0);
        }
        protected bool GetBit(uint val)
        {            
            return (val & EncryptBit) > 0;
        }

        protected int width_2n;
        protected int height_2n;
        protected Bitmap image;
        public Bitmap Image { get => image; set => image = value; }

        public override string ToString()
        {
            return "AbstractDigitalWatermarkingDWT";
        }

        public void Clear()
        {
            for (int i = 0, height = image.Height, width = image.Width; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    //SetPixelInfo(i, j, 0);
                }
            }
        }
    }
}
