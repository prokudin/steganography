﻿using System.Drawing;

namespace Steganography.Analysis
{
    /// <summary>
    /// Интерфейс, описывающий метод анализа изображения
    /// </summary>
    public interface IAnalysisMethod
    {
        double Analyse(Bitmap first, Bitmap second);        
    }
}
