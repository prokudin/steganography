﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Steganography.Analysis
{
    class PeakSignalToNoiseRatio : MeanSquareError
    {
        public override double Analyse(Bitmap first, Bitmap second)
        {
            double tmp = 1 / (base.Analyse(first, second) / (3 * 255 * 255));

            return 10 * Math.Log10(tmp);
        }

        public override string ToString()
        {
            return "PSNR";
        }
    }
}
