﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Steganography.Analysis
{
    class RootMeanSquare : MeanSquareError
    {
        public override double Analyse(Bitmap first, Bitmap second)
        {
            return Math.Sqrt(base.Analyse(first, second));
        }

        public override string ToString()
        {
            return "RMS";
        }
    }
}
